function iris_visible_bm_er = remove_inferior_eyelid(img, iris_visible_best_mask, iris_mask, sigma)
debug = false;

[h, w] = size(iris_mask);

iris = img .* repmat(uint8(iris_visible_best_mask),1,1,3);
iris_gray = iris(:,:,1);

iris_diffgauss = iris_gray - imgaussfilt(iris_gray, sigma);
if(debug), figure, subplot(3,1,1), imshow(iris_diffgauss); end;

% cut the iris_difgauss and keep only the bottom thirth
sm = regionprops(iris_visible_best_mask, 'Eccentricity', 'Centroid', 'Area', 'PixelIdxList', 'BoundingBox');
hi = sm.BoundingBox(4);
wi = sm.BoundingBox(3);
top_w = sm.BoundingBox(1);
top_h = sm.BoundingBox(2);
bottom_h = sm.BoundingBox(2) + hi;
bottom_3rd_ini = ceil(top_h + 0.66*hi);
bottom_3rd_mask = poly2mask([top_w top_w+wi top_w+wi top_w], [bottom_3rd_ini bottom_3rd_ini bottom_h bottom_h], h, w);

iris_diffgauss_cropped = iris_diffgauss .* uint8(bottom_3rd_mask);

th = graythresh(iris_diffgauss_cropped);%*1.1;
spec_ref_and_eyelid_otsu = (im2bw(iris_diffgauss_cropped, th));
% smooth it
spec_ref_and_eyelid_otsu_smooth = logical(imgaussfilt(uint8(imfill(spec_ref_and_eyelid_otsu, 'holes')), 1));

if(debug), subplot(3,1,2), imshow(spec_ref_and_eyelid_otsu_smooth); end;
if(debug), subplot(3,1,3), imshow(uint8(~bwperim(spec_ref_and_eyelid_otsu_smooth)).*img(:,:,1)); end;

s = regionprops(spec_ref_and_eyelid_otsu_smooth, 'Eccentricity', 'Centroid', 'Area', 'PixelIdxList', 'Image');
% eyelid must have high eccentricity
ecc = [s(:).Eccentricity];
if(isempty(ecc)), ecc = 0; end;
% centroid close to the top of the iris detection region
centroids = [s(:).Centroid]';
if(isempty(centroids)), centroids = [0; 0]; else reshape(centroids, 2, numel(ecc)); end;
% and areas quite big
areas = [s(:).Area];
if(isempty(areas)), areas = 0; end;
% bbox = [s(:).BoundingBox];

% check if its a good mask or not (if top half has less than 0.8 bottom half)
middle_third_mask = round(bottom_3rd_ini + (bottom_h-bottom_3rd_ini)/2);
bottom_3rd_mask_top = poly2mask([top_w top_w+wi top_w+wi top_w], [bottom_3rd_ini bottom_3rd_ini middle_third_mask middle_third_mask], h, w);
bottom_3rd_mask_bottom = poly2mask([top_w top_w+wi top_w+wi top_w], [middle_third_mask middle_third_mask bottom_h bottom_h], h, w);
det_mask = spec_ref_and_eyelid_otsu_smooth & middle_third_mask;
det_mask_top = bottom_3rd_mask_top & spec_ref_and_eyelid_otsu_smooth;
det_mask_bottom = bottom_3rd_mask_bottom & spec_ref_and_eyelid_otsu_smooth;
n_px_mask_top = sum(det_mask_top(:) > 0);
n_px_mask_bottom = sum(det_mask_bottom(:) > 0);
n_px_total_top = sum(bottom_3rd_mask_top(:) > 0);
n_px_total_bottom = sum(bottom_3rd_mask_bottom(:) > 0);

det_mask_desp = false(h, w);
[idet, jdet] = find(det_mask);
det_mask_desp(sub2ind([h w], idet - round((bottom_h-bottom_3rd_ini)/2), jdet)) = 1;

color_iris = mean(iris_gray(logical(iris_visible_best_mask)));
color_iris_sd = std(double(iris_gray(logical(iris_visible_best_mask))));
color_det_mask = mean(iris_gray(det_mask));
color_det_mask_desp = mean(iris_gray(det_mask_desp));

color_px_2_remove = color_det_mask;

eyelids_ok = ecc > 0.7 & ...
    areas >= pi*3^2 & areas <= 0.66*wi*(bottom_h-bottom_3rd_ini) & ...
    centroids(2,:) > (top_h + hi * 0.75) & ...
    ((n_px_mask_top < 0.9*n_px_mask_bottom & ...
    n_px_mask_top/n_px_total_top < n_px_mask_bottom/n_px_total_bottom & ...
    n_px_mask_top/n_px_total_top < 0.5 & ...
    color_px_2_remove > 75 & ...
    abs(color_det_mask-color_det_mask_desp) > 0.1*255) ...
    || ...
    color_px_2_remove > 130); %0.75*color_iris_sd; % 0.75 doesn't make normal fail but misses some bad inferior eyelids
% eyelids_ok

% eyelids_ok = ecc > 0.7 & ...
%     areas >= pi*3^2 & areas <= 0.5*wi*(bottom_h-bottom_3rd_ini) & ...
%     centroids(2,:) > (top_h + hi * 0.75) & ...
%     n_px_mask_top < 0.9*n_px_mask_bottom & ...
%     n_px_mask_top/n_px_total_top < n_px_mask_bottom/n_px_total_bottom & ...
%     n_px_mask_top/n_px_total_top < 0.5 & ...
%     color_px_2_remove > 75;

% % check if its a good mask or not (if has a lot of pixels on the top of
% % bottom_3rd_mask it is not a good mask)
% d=5;
% bottom_3rd_mask_1st5th_row = poly2mask([top_w top_w+wi top_w+wi top_w], [bottom_3rd_ini bottom_3rd_ini bottom_3rd_ini+d bottom_3rd_ini+d], h, w);
% n_pixels_on_1st5th_mask_row = bottom_3rd_mask_1st5th_row & spec_ref_and_eyelid_otsu_smooth;
% n_total = sum(bottom_3rd_mask_1st5th_row(:) > 0);
% n_mask = sum(n_pixels_on_1st5th_mask_row(:) > 0);
% 
% eyelids_ok = ecc > 0.7 & ...
%     areas >= pi*3^2 & areas <= 0.5*wi*(bottom_h-bottom_3rd_ini) & ...
%     centroids(2,:) > (top_h + hi * 0.75) & ...
%     n_mask < 0.25*n_total;

iris_visible_bm_er = iris_visible_best_mask;
if(sum(eyelids_ok(:)))
    eyelids_pxls = {s(eyelids_ok).PixelIdxList};
    eyelids_pxls = cell2mat(eyelids_pxls');
    mask_eyelids = false(h,w);
    mask_eyelids(eyelids_pxls) = true;
    
    % make a little bigger
    % mask_eyelids_bigger = imdilate(mask_eyelids, strel('disk', 1));
    % smooth mask
    %     mask_eyelids_smooth = imclose(imclose(mask_eyelids, strel('line', 10, 0)), strel('disk', 2));
    %     if(debug), figure, imshow(uint8(~bwperim(mask_eyelids_smooth)).*img(:,:,1)); end;
        
    % now, finally remove superior eyelid
    iris_visible_bm_er(mask_eyelids>0) = 0;
    
    % take the convex hull
    iris_visible_bm_er = convex_hull_centers(iris_visible_bm_er);
end

if(debug), figure;imshow(img(:,:,1).* uint8(iris_visible_bm_er)); end;