function ch = convex_hull_centers(bw)

[h,w] = size(bw);

[y,x] = find(bw);

dx = [0.0  0.0  0.5 -0.5];
dy = [0.5 -0.5  0.0  0.0];

x_edges = bsxfun(@plus, x, dx);
y_edges = bsxfun(@plus, y, dy);

x_edges = x_edges(:);
y_edges = y_edges(:);

k = convhull(x_edges, y_edges);

x_hull = x_edges(k);
y_hull = y_edges(k);

ch = poly2mask(x_hull, y_hull, h, w);