function iris_visible_best_mask = choose_iris_mask(iris_visible_mask_closed, iris_mask, iter_max)

if(~exist('iter_max', 'var')), iter_max = 2; end

[h, w] = size(iris_mask);

b = bwperim(iris_visible_mask_closed);
b0 = bwperim(iris_mask);

s = regionprops(b, 'all');
s0 = regionprops(b0, 'all');

if(numel(s) == 0)
    iris_visible_best_mask = iris_mask;
    return;
end

left_w = round(s0.BoundingBox(1) + 3*s0.BoundingBox(3)/4);
right_w = round(s0.BoundingBox(1) + s0.BoundingBox(3)/2/4);
middle_w = round(s0.BoundingBox(1) + s0.BoundingBox(3)/2);
% middle_h = round(s0.BoundingBox(2) + s0.BoundingBox(4)/2);
bottom0 = s0.BoundingBox(2) + s0.BoundingBox(4);
bottom = s.BoundingBox(2) + s.BoundingBox(4);
top = s.BoundingBox(2);

bottomleftpoint = max(s.PixelList(s.PixelList(:,1)==left_w,2));
bottommiddlepoint = max(s.PixelList(s.PixelList(:,1)==middle_w,2));
bottomrightpoint = max(s.PixelList(s.PixelList(:,1)==right_w,2));
% bottommiddlepoint0 = round(s0.BoundingBox(2) + s0.BoundingBox(4));

if(isempty(bottomleftpoint)), bottomleftpoint = 1e4; end
if(isempty(bottommiddlepoint)), bottommiddlepoint = 1e4; end
if(isempty(bottomrightpoint)), bottomrightpoint = 1e4; end

% if(abs(bottommiddlepoint-bottommiddlepoint0) > 10 && iter_max > 0)
if(max([bottomleftpoint bottommiddlepoint bottomrightpoint])-min([bottomleftpoint bottommiddlepoint bottomrightpoint]) > 12 ...
        && iter_max > 0)
%         && abs(bottommiddlepoint-bottommiddlepoint0) > 10 ...
    % try to fix the iris segmented edge taking the lower half of the iris
    % border and pasting into it
    h_to_start_cpy = top + 0.33*(bottom-top);
    
    patch_mask = poly2mask([s0.BoundingBox(1)-2 s0.BoundingBox(1)+s0.BoundingBox(3)+2 s0.BoundingBox(1)+s0.BoundingBox(3)+2 s0.BoundingBox(1)-2], ...
        [h_to_start_cpy h_to_start_cpy bottom0 bottom0], h, w);
    ind = find(patch_mask == 1);
    b_corr = b;
    b_corr(ind) = b0(ind);
    
    % join borders
    b_corr = bwmorph(bwmorph(b_corr, 'fatten', 5), 'thin', 'inf');
    
    iris_visible_best_mask = choose_iris_mask(imfill(b_corr, 'holes'), iris_mask, iter_max-1);
else
    iris_visible_best_mask = iris_visible_mask_closed;
end