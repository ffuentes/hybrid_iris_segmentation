function seg_eval = run_iris_segmentation_batch(i_rmin, i_rmax, th_bin_mult, th_sr_mult, sigma, folder_imgs, folder_results, verbose)

addpath('.\functions\');
addpath('.\functions\DaugmanIDO');

folder_imgs_default = '.\images\';
folder_results_default = '.\results\';

% % iris parameters
switch(nargin)
    case 0
        i_rmin = 25;
        i_rmax = 40;
        th_bin_mult = 1.1;
        th_sr_mult = 0.95;
        sigma = 9.5;
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 1
        i_rmax = 40;
        th_bin_mult = 1.1;
        th_sr_mult = 0.95;
        sigma = 9.5;
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 2
        th_bin_mult = 1.1;
        th_sr_mult = 0.95;
        sigma = 9.5;
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 3
        th_sr_mult = 0.45;
        sigma = 5;
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 4
        sigma = 5;
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 5
        folder_imgs = folder_imgs_default;
        folder_results = folder_results_default;
        verbose = false;
    case 6
        folder_results = folder_results_default;
        verbose = false;
    case 7
        verbose = false;
end

folder_original_gts = '.\cfd_gt\';
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;
if(~exist(fullfile(folder_results, 'gt'), 'dir')), mkdir(fullfile(folder_results, 'gt')); end;

extension = '.jpg';

% list all 'extension' files in 'folder_imgs'
img_filenames = dir([folder_imgs '*' extension]);
n_img_filenames = numel(img_filenames);

% % Load models to detect landmarks
tic
disp('Loading models for facial feature detection...');
models = loadModels();
toc

% % Variable to store timestamps to measure speed
tmstmps.header = {'load_image', 'landmarks', 'masks', 'segment', 'all'};
tmstmps.t = zeros(n_img_filenames, 5);

% % To store segmentation evaluation
seg_eval = zeros(n_img_filenames*2, 5);

h = waitbar(0, 'Processing image... 0%');
t=tic;
ni = 1;
% start the loop for every image in the list
for nf=1:n_img_filenames
    
    waitbar(nf/n_img_filenames, h, sprintf('Processing image... %.2f%%', nf/n_img_filenames*100));
    disp(['Processing image ' num2str(nf) '/' num2str(n_img_filenames)]);
    
    % % Image filename
    image_name = strrep(img_filenames(nf).name, extension, '');
    
    % % Load image
    t1 = tic;
    img = imread(fullfile(folder_imgs, [image_name extension]));
    tmstmps.t(nf, 1) = toc(t1);
    
    t2 = tic;
    % % Get landmarks
    disp('Getting landmarks...');
    landmarks = getLandmarksFromImg(img, image_name, models, verbose);
    tmstmps.t(nf, 2) = toc(t2);
    
    t3 = tic;
    % % Get masks
    disp('Getting masks...');
    masks = getMasks(img, landmarks, verbose);
    tmstmps.t(nf, 3) = toc(t3);
    
    % % Segment the iris
    t4 = tic;
    disp('Segmenting the iris...');
    iris = segment_iris(img, landmarks, masks, i_rmin, i_rmax, th_bin_mult, th_sr_mult, sigma, verbose);
    tmstmps.t(nf, 4) = toc(t4);
    tmstmps.t(nf, 5) = toc(t1);
    
    % % Save segmented mask
    imwrite(iris.l.visible_mask, fullfile(folder_results, [image_name '_L_smask.bmp']));
    imwrite(iris.r.visible_mask, fullfile(folder_results, [image_name '_R._smask.bmp']));
    
    % % Save segmented mask on eye image
    imwrite(iris.l.out, fullfile(folder_results, [image_name '_L.bmp']));
    imwrite(iris.r.out, fullfile(folder_results, [image_name '_R.bmp']));
    
    % % Save GT cropped to match iris segmentation output size
    GT = crop_gt_to_match_segmentation_output(imread(fullfile(folder_original_gts, [image_name '.bmp'])), masks, fullfile(folder_results, ['gt\' image_name]));
    
    % % Evaluate
    % % Left eye
    [JC,DC,Pr,Rc,Err] = segeval(iris.l.visible_mask,GT.el);
    seg_eval(ni, 1) = JC;
    seg_eval(ni, 2) = DC;
    seg_eval(ni, 3) = Pr;
    seg_eval(ni, 4) = Rc;
    seg_eval(ni, 5) = Err;
    % % Right eye
    ni = ni+1;
    [JC,DC,Pr,Rc,Err] = segeval(iris.r.visible_mask,GT.er);
    seg_eval(ni, 1) = JC;
    seg_eval(ni, 2) = DC;
    seg_eval(ni, 3) = Pr;
    seg_eval(ni, 4) = Rc;
    seg_eval(ni, 5) = Err;
    ni = ni+1;
end
disp('Execution ended.');
toc(t)

save(fullfile(folder_results, 'tmstmps.mat'), 'tmstmps');
close(h);