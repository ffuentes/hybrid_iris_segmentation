function iris = segment_iris(img, landmarks, masks, verbose)

show_detection_steps = true;
save_detection_steps = false;

% Create mask for both eyes
e_mask = uint8(masks.mask_RE | masks.mask_LE);

% Convert image to gray and apply eye's mask
img_gray = rgb2gray(img) .* e_mask;
% Opponent color works worse for iris contour (circle)

% Find bounding box to crop the image
props = regionprops(logical(e_mask), 'BoundingBox');

% Find coordinates
x1= floor(props(1).BoundingBox(2)); dx1 = ceil(props(1).BoundingBox(4));
y1 = floor(props(1).BoundingBox(1)); dy1 = ceil(props(1).BoundingBox(3));
x2 = floor(props(2).BoundingBox(2)); dx2 = ceil(props(2).BoundingBox(4));
y2 = floor(props(2).BoundingBox(1)); dy2 = ceil(props(2).BoundingBox(3));

% Correct landmarks for cropped image with only eyes
er_lm = landmarks(37:42, :) - repmat([y1 x1], 6, 1);
el_lm = landmarks(43:48, :) - repmat([y2 x2], 6, 1);

% Crop images (gray and color)
erg = img_gray(x1:x1+dx1, y1:y1+dy1, :);
elg = img_gray(x2:x2+dx2, y2:y2+dy2, :);
er = img(x1:x1+dx1, y1:y1+dy1, :);
el = img(x2:x2+dx2, y2:y2+dy2, :);

%------------------------------------------------------------------------->
% FIRST STEP: Find iris actual radius and center, and eye's area
%------------------------------------------------------------------------->

% Minimum and maximum right and left eye's radii
er_rmin = 25; er_rmax = 40;
el_rmin = 25; el_rmax = 40;

% Find the center and radius of the complete iris
[~, ~, er_iris_out, er_iris_mask] = find_iris_contour(erg, er_rmin, er_rmax, er_lm);
[~, ~, el_iris_out, el_iris_mask] = find_iris_contour(elg, el_rmin, el_rmax, el_lm);

% Plot detections
if(verbose)
    figure;
    subplot(1,2,1), imshow(er_iris_out)
    subplot(1,2,2), imshow(el_iris_out)
end

%------------------------------------------------------------------------->
% SECOND STEP: Find visible iris area
%------------------------------------------------------------------------->

iris.r = find_iris_visible_area_otsu(er, er_iris_mask, verbose);
iris.l = find_iris_visible_area_otsu(el, el_iris_mask, verbose);
