function [masks, IOD, thicken]=getMasks(img, landmarks, verbose)


% % CHEHRA Landmarks labels
lm.chehra.LeftEyebrow = 1:5;
lm.chehra.RightEyebrow = 6:10;
lm.chehra.LeftEye = 20:25;
lm.chehra.RightEye = 26:30;
lm.chehra.Nose = [11, 15, 19]; % 11:14;
lm.chehra.Nostrils = 15:19;
lm.chehra.OutterMouth = 32:43;

% % Load image
test_image = im2double(img);
h = size(test_image, 1); w = size(test_image, 2);

% % Find centroids of Left and Right eyes
[~, cxLE, cyLE] = polycenter(landmarks((20:25),1), landmarks((20:25),2));
[~, cxRE, cyRE] = polycenter(landmarks((26:31),1), landmarks((26:31),2));

% % Eucledian IOD
IOD = sqrt((cxLE-cxRE)^2 + (cyLE-cyRE)^2);

% % Create masks for each facial component and draw limits
thicken = 0.2*IOD;
masks = createMasks(test_image, landmarks, lm.chehra, thicken); %72px thicken
[ob_i, ob_j] = find(masks.smooth.total_outter_border);

% % Show all detected masks
if(verbose)
    % close all
    figure1 = figure;
    imshow(test_image); %title(strrep(img_file, '_', '-'))
    hold on;
    plot(ob_j, ob_i, 'b.', 'MarkerSize', 1)
end

% % Find mask-feature correspondances
n_detected_masks = max(masks.smooth.total_outter(:));
cx = nan(n_detected_masks, 1); % centroids (x coord)
cy = nan(n_detected_masks, 1); % centroids (y coord)
fm = zeros(h, w, n_detected_masks); % feature masks

% Find centroids and Area of each region existing in the mask and
% create a mask for each region individually
for im=1:n_detected_masks
    [ebi, ebj] = find(masks.smooth.total_outter == im);
    m = zeros(h, w);
    m(sub2ind([h w], ebi, ebj))=1;
    fm(:, :, im) = m;
    b = bwboundaries(m);
    [~, cy(im), cx(im)] = polycenter(b{1,1}(:,1), b{1,1}(:,2));
    
    if(verbose), plot(cx(im), cy(im), '.r', 'MarkerSize', 10); end
end

% % Decide which mask corresponds with which feature (notice that x and
% % y in an image start counting from top left corner, x > down, y > right)
% % Mouth
try
    % Mouth mask (downmost)
    ind_M = find(cy == max(cy));
    masks.mask_M = fm(:, :, ind_M);
    euclidean_distance = sqrt((cy(ind_M)-cy).^2+(cx(ind_M)-cx).^2);
    [~, d_to_mouth_y] = sort(euclidean_distance); % EUCLIDEAN distance of all features to the mouth (x, y coords)
    % [~, d_to_mouth_y] = sort(abs(cy(ind_M)-cy)); % distance of all features to the mouth (y coord)
    
    % % Nose
    ind_N = d_to_mouth_y(2); % 1st is mouth itself, second has to be nose
    masks.mask_N = fm(:, :, ind_N); % Nose mask (closest to mouth in y)
    
    % % Eyes
    ind_E1 = d_to_mouth_y(3);
    ind_E2 = d_to_mouth_y(4);
    if(cx(ind_E1) < cx(ind_E2))
        ind_LE = ind_E2;
        ind_RE = ind_E1;
    else
        ind_LE = ind_E1;
        ind_RE = ind_E2;
    end
    masks.mask_LE = fm(:, :, ind_LE);
    masks.mask_RE = fm(:, :, ind_RE);
    
    % % Eyebrows
    [~, d_to_left_eye_x] = sort(abs(cx(ind_LE)-cx)); % distance of all features to the left eye (x coord)
    [~, d_to_right_eye_x] = sort(abs(cx(ind_RE)-cx)); % distance of all features to the right eye (x coord)
    ind_LEB = d_to_left_eye_x(2);
    ind_REB = d_to_right_eye_x(2);
    masks.mask_LEB = fm(:, :, ind_LEB);
    masks.mask_REB = fm(:, :, ind_REB);
    
catch e
    disp(e.getReport());
end