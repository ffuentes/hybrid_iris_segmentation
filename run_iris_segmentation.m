function [iris_mask_R, iris_mask_L, seg_eval] = run_iris_segmentation(img_path)

if(nargin < 1)
    img_path = '.\images\CFD-AM-201-076-N.jpg';
end

addpath('.\functions\');
addpath('.\functions\DaugmanIDO');

folder_results = '.\results\';

i_rmin = 25;
i_rmax = 40;
th_bin_mult = 1.1;
th_sr_mult = 0.95;
sigma = 9.5;
verbose = false;

folder_original_gts = '.\cfd_gt\';
if(~exist(folder_results, 'dir')), mkdir(folder_results); end;
if(~exist(fullfile(folder_results, 'gt'), 'dir')), mkdir(fullfile(folder_results, 'gt')); end;

extension = '.jpg';

% % Load models to detect landmarks
tic
disp('Loading models for facial feature detection...');
models = loadModels();
toc

% % To store segmentation evaluation
seg_eval = zeros(2, 5);

% start the loop for every image in the list
disp('Processing image...');

% % Image filename
image_name_aux =strsplit(img_path, '\');
image_name = strrep(image_name_aux{end}, extension, '');

% % Load image
img = imread(img_path);

% % Get landmarks
disp('Getting landmarks...');
landmarks = getLandmarksFromImg(img, image_name, models, verbose);

% % Get masks
disp('Getting masks...');
masks = getMasks(img, landmarks, verbose);

% % Segment the iris
disp('Segmenting the iris...');
iris = segment_iris(img, landmarks, masks, i_rmin, i_rmax, th_bin_mult, th_sr_mult, sigma, verbose);

% % Save segmented mask
imwrite(iris.l.visible_mask, fullfile(folder_results, [image_name '_L_smask.bmp']));
imwrite(iris.r.visible_mask, fullfile(folder_results, [image_name '_R._smask.bmp']));

% % Save segmented mask on eye image
imwrite(iris.l.out, fullfile(folder_results, [image_name '_L.bmp']));
imwrite(iris.r.out, fullfile(folder_results, [image_name '_R.bmp']));
iris_mask_L = iris.l.visible_mask;
iris_mask_R = iris.r.visible_mask;

% % Save GT cropped to match iris segmentation output size
GT = crop_gt_to_match_segmentation_output(imread(fullfile(folder_original_gts, [image_name '.bmp'])), masks, fullfile(folder_results, ['gt\' image_name]));

% % Evaluate
% % Left eye
[JC,DC,Pr,Rc,Err] = segeval(iris.l.visible_mask,GT.el);
seg_eval(1, 1) = JC;
seg_eval(1, 2) = DC;
seg_eval(1, 3) = Pr;
seg_eval(1, 4) = Rc;
seg_eval(1, 5) = Err;
% % Right eye
[JC,DC,Pr,Rc,Err] = segeval(iris.r.visible_mask,GT.er);
seg_eval(2, 1) = JC;
seg_eval(2, 2) = DC;
seg_eval(2, 3) = Pr;
seg_eval(2, 4) = Rc;
seg_eval(2, 5) = Err;

disp('Execution ended.');