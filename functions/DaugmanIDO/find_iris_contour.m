% Function to search for the centre coordinates of the pupil and the iris
% along with their radii
% It makes use of Camus&Wildes' method to select the possible centre 
% coordinates first. The method consist of thresholding followed by
% checking if the selected points(by thresholding) correspond to a local 
% minimum in their immediate (3*s) neighbourhood.
% These points serve as the possible centre coordinates for the iris.
% Once the iris has been detected (using Daugman's method) the pupil's 
% centre coordinates are found by searching a 10*10 neighbourhood around 
% the iris centre and varying the radius until a maximum is found (using 
% Daugman's integrodifferential operator)
% INPUTS:
% I: image to be segmented
% rmin, rmax: the minimum and maximum values of the iris radius
% OUTPUTS:
% ic: iris center
% ir: iris radius
% out: the segmented image
% mask: binary mask of the iris
% Author: Anirudh S.K.
% Department of Computer Science and Engineering
% Indian Institute of Techology, Madras
% Modified by:  F�lix F.H.
%               Polytechnical University of Valencia

function [ic, ir, out, mask, eye_area, eye_mask] = find_iris_contour(I, rmin, rmax, lm)
% function [ci, cp, out] = find_iris_contour(I, rmin, rmax, lm)
% save test
% load test

% Arithmetic operations are not defined on uint8 -> convert to double
I = im2double(I);
% Stores the image for display
pimage = I;

% Scale image and parameters to speed up the process
scale=1;
rmin = rmin * scale;
rmax = rmax * scale;
I = imresize(I, scale);
[h, w] = size(I);

% Area of the eye
eye_mask = uint8(zeros(h, w));
eye_mask = roipoly(eye_mask, lm(:,1), lm(:,2));
eye_area = sum(eye_mask(:));

% Map where the pupil can be located
map = uint8(zeros(h, w));
map = roipoly(map, lm([2 3 5 6],1), lm([2 3 5 6],2));
map = imdilate(map, strel('line', 10, 30));
% map = roipoly(map, lm(:,1), lm(:,2));
% map = imdilate(map, strel('disk', 1));

% Remove specular reflections
I = imcomplement(imfill(imcomplement(I), 'holes'));

% Generate (x,y) of the image elements that are inside the map
[X, Y] = find(map);
% Generate a column vector of the image elements that have been selected by
% tresholding; one for x coordinate and one for y
% [X, Y] = find(I < 0.5);

rows = size(I, 1);
cols = size(I, 2);
s = size(X,1);

for k=1:s
    % This process scans the neighbourhood of the selected pixel to check 
    % if it is a local minimum
    % below "if": not needed -> map used
    % if (X(k) > rmin) && (Y(k) > rmin) && (X(k) <= (rows-rmin)) && (Y(k) < (cols-rmin))
        A = I((X(k)-1):(X(k)+1), (Y(k)-1):(Y(k)+1));
        M = min(A(:));
        
        if I(X(k), Y(k)) ~= M
            X(k) = NaN;
            Y(k) = NaN;
        end
    % end
end

% Delete all pixels that are NOT local minima (that have been set to NaN)
v = find(isnan(X));
X(v) = [];
Y(v) = [];

% Size after deleting unnecessary elements
N = size(X, 1);

% Define two arrays "maxb" and "maxrad" to store the maximum value of blur
% for each of the selected centre points and the corresponding radius
maxb = zeros(rows, cols);
maxrad = zeros(rows, cols);

for j=1:N
    % Coarse search. 3rd parameter is blur (b for all radii)
    [b, r, ~] = partiald(I, [X(j) Y(j)], rmin, rmax, 'inf', 600, 'iris');
    maxb(X(j), Y(j)) = b;
    maxrad(X(j), Y(j)) = r;
end

% Find the maximum value of blur by scanning all the centre coordinates
[x, y] = find(maxb == max(maxb(:)));
% Only one value must be kept, and it cannot be less than 6, 6 (due to search
% script limitation)
x = x(1);
y = y(1);
if(x < 10), x = round(cols/2); end;
if(y < 10), y = round(rows/2); end;

% Fine search. In the previous search not every point was considered, only
% the local minima found taking 3x3 regions centered in the potential centers. 
% Now, all pixels in the neighbourhood of the local minima are checked and 
% if a better one is found, it is selected.
ci = search(I, [x y], rmin, rmax, 0.5, 600, 'iris'); 
ci = ci/scale;

% This is to find the pupil and is not used.
% The function search searches for the centre of the pupil and its radius
% by scanning a 10*10 window around the iris centre for establishing the 
% pupil's centre and hence its radius
% Ref: Daugman's paper that sets biological limits on the relative sizes of the iris and pupil
% cp = search(I, round(0.1*r), round(0.8*r), ci(1)*scale, ci(2)*scale, 'pupil');
% cp = cp/scale;


% Get output parameters ready
ic = ci(1:2);
ir = ci(3);
mask = createmask(pimage, ic, ir, 600);
% Display the segmented image
out = drawcircle(pimage, ic, ir, 600);
% out = drawcircle(out, [cp(1) cp(2)], cp(3), 600);
