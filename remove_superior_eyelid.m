function iris_visible_bm_ser = remove_superior_eyelid(img, iris_visible_best_mask, iris_mask, sigma)
debug = false;

[h, w] = size(iris_mask);

iris = img .* repmat(uint8(iris_visible_best_mask),1,1,3);
iris_gray = iris(:,:,1);

iris_diffgauss = iris_gray - imgaussfilt(iris_gray, sigma);
if(debug), figure, subplot(3,1,1), imshow(iris_diffgauss); end;

% cut the iris_difgauss and keep only the top thirth
sm = regionprops(iris_visible_best_mask, 'Eccentricity', 'Centroid', 'Area', 'PixelIdxList', 'BoundingBox');
hi = sm.BoundingBox(4);
wi = sm.BoundingBox(3);
top_w = sm.BoundingBox(1);
top_h = sm.BoundingBox(2);
top_3rd_end = ceil(top_h + 0.33*hi);
top_3rd_mask = poly2mask([top_w top_w+wi top_w+wi top_w], [top_h top_h top_3rd_end top_3rd_end], h, w);

iris_diffgauss_cropped = iris_diffgauss .* uint8(top_3rd_mask);

th = graythresh(iris_diffgauss_cropped);
spec_ref_and_eyelid_otsu = (im2bw(iris_diffgauss_cropped, th));
% smooth it
spec_ref_and_eyelid_otsu_smooth = logical(imgaussfilt(uint8(imfill(spec_ref_and_eyelid_otsu, 'holes')), 1));

if(debug), subplot(3,1,2), imshow(spec_ref_and_eyelid_otsu_smooth); end;
if(debug), subplot(3,1,3), imshow(uint8(~bwperim(spec_ref_and_eyelid_otsu_smooth)).*img(:,:,1)); end;

s = regionprops(spec_ref_and_eyelid_otsu_smooth, 'Eccentricity', 'Centroid', 'Area', 'PixelIdxList');

% eyelid must have high eccentricity
ecc = [s(:).Eccentricity];
if(isempty(ecc)), ecc = 0; end;
% centroid close to the top of the iris detection region
centroids = [s(:).Centroid]';
if(isempty(centroids)), centroids = [0; 0]; else reshape(centroids, 2, numel(ecc)); end;
% and areas quite big
areas = [s(:).Area];
if(isempty(areas)), areas = 0; end;
% bbox = [s(:).BoundingBox];

eyelids_ok = ecc > 0.7 & ...
    areas >= pi*3^2 & areas <= pi*(0.3*max([hi wi]))^2 & ...
    centroids(2,:) < (top_h + hi * 0.25);

iris_visible_bm_ser = iris_visible_best_mask;

if(sum(eyelids_ok(:)))
    eyelids_pxls = {s(eyelids_ok).PixelIdxList};
    eyelids_pxls = cell2mat(eyelids_pxls');
    mask_eyelids = false(h,w);
    mask_eyelids(eyelids_pxls) = true;
    
    % make a little bigger
    mask_eyelids_bigger = imdilate(mask_eyelids, strel('disk', 2));
    
    % now, finally remove superior eyelid
    iris_visible_bm_ser(mask_eyelids_bigger>0) = 0;
    
    % take the convex hull
    iris_visible_bm_ser = convex_hull_centers(iris_visible_bm_ser);
end

if(debug), figure;imshow(img(:,:,1).* uint8(iris_visible_bm_ser)); end;