function m = loadModels()

% % Algorithm folder
m.alg_folder = './functions/';

% % Add Chehra folder to the path
m.chehra_folder = fullfile(m.alg_folder, 'Chehra_v0.1_MatlabFit');
addpath(genpath(m.chehra_folder));
% % ---------------------------------------------------------------------->

% ####################################################################### %
% ################  Use CHEHRA to detect facial features  ############### %
% ####################################################################### %

% % CHEHRA Landmarks labels
lm.chehra.LeftEyebrow = 1:5;
lm.chehra.RightEyebrow = 6:10;
lm.chehra.LeftEye = 20:25;
lm.chehra.RightEye = 26:30;
lm.chehra.Nose = [11, 15, 19]; % 11:14;
% lm.chehra.Nostrils = 15:19;
lm.chehra.OutterMouth = 32:43;
% lm.chehra.InnerMouth = 44:49;
% % ---------------------------------------------------------------------->

m.lm = lm;

% % Load Chehra Model
m.fitting_model=fullfile(m.chehra_folder, 'models/Chehra_f1.0.mat');
load(m.fitting_model);
m.refShape = refShape;
m.RegMat = RegMat;
% % ---------------------------------------------------------------------->