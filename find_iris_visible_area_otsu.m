function iris = find_iris_visible_area_otsu(img, iris_mask, th_bin_mult, th_sr_mult, sigma, verbose)
irisr = img(:,:,1);

if(verbose), f=figure; subplot(3,2,1), imshow(irisr), title('original'), end;
[h, w] = size(irisr);

% Remove specular reflections
iris.wo_sr = remove_iris_specular_reflections(img, iris_mask, th_sr_mult, sigma);
if(verbose), subplot(3,2,2), imshow(iris.wo_sr); title('iris wo sr'); end;

% Close holes
iris.ch = close_holes(iris.wo_sr);
% if(verbose), subplot(3,2,3), imshow(iris.ch); title('iris ch'); end;

% Calculate the residue (image with holes closed - original image)
iris.res = iris.ch - iris.wo_sr;
if(verbose), subplot(3,2,3), imshow(iris.res); title('iris res'); end;

% Binarize using otsu method to obtain the threshold (th_bin_mult)
iris.otsu = im2bw(iris.res, th_bin_mult*graythresh(iris.res)) & iris_mask;
if(verbose), subplot(3,2,4), imshow(iris.otsu); title('iris otsu'); end;

% Close holes
iris.otsu_closed = imclose(imclose(iris.otsu, strel('line', 20, 45)), strel('line', 20, 315));
% And remove them by filling them
iris.otsu_fill = imfill(iris.otsu_closed, 'holes');

% Remove peaks and non desirable 
iris.otsu_pruned = imopen(iris.otsu_fill, strel('disk', 5));
if(verbose), subplot(3,2,5), imshow(iris.otsu_pruned); title('iris ch filled and pruned'); end;

% Apply mask to binarized image to keep only the iris area
iris.iris.visible_mask_v0 = imfill(iris.otsu_pruned & iris_mask, 'holes');
% if(verbose), subplot(3,2,5), imshow(iris.visible_mask); title('iris visible mask'); end;

% Remove spurious detections
props = regionprops(iris.iris.visible_mask_v0, 'Area', 'PixelIdxList');
if(isempty(props))
    iris.iris.visible_mask_biggest = iris.iris.visible_mask_v0;
else
    [~, order] = sort([props(:).Area], 'descend');
    iris.iris.visible_mask_biggest = false(h, w);
    iris.iris.visible_mask_biggest(props(order(1)).PixelIdxList) = true;
end

% Close any possible hole present in the mask
% iris.visible_mask_closed = close_shapes(iris.visible_mask);
% iris.visible_mask_closed = bwconvhull(iris.visible_mask_clean);
% Convex hull from center of pixel, not corners
try
    iris.iris.visible_mask_convex = convex_hull_centers(iris.iris.visible_mask_biggest) .* iris_mask;
    area0 = regionprops(iris_mask, 'Area');
    area = regionprops(iris.iris.visible_mask_convex, 'Area');
    if(area(1).Area < 0.6*area0(1).Area)
%         iris.visible_mask_closed = iris_mask;
    end
catch
    iris.iris.visible_mask_convex = iris.iris.visible_mask_biggest; %iris_mask;
end

% Check if iris.visible_mask_closed is well detected enough to use it. If 
% not, use iris_mask instead.
iris.visible_best_mask = choose_iris_mask(iris.iris.visible_mask_convex, iris_mask);

% try to remove the superior eyelid
iris.visible_bm_ser = remove_superior_eyelid(img, iris.visible_best_mask, iris_mask, sigma);

% try to remove the inferior eyelid (FINAL MASK)
iris.visible_mask = remove_inferior_eyelid(img, iris.visible_bm_ser, iris_mask, sigma);

% Get the border of the detected area
iris.visible_border = bwperim(iris.visible_mask);

% Remove eyelids (returns the border of the corrected area)
% iris.visible_border_corr = remove_eyelids_from_iris_detection(irisr, iris_mask, iris.visible_mask_closed);
iris.visible_border_corr = iris.visible_border;

% Create a 3D mask to apply to the RGB image
% iris_mask_3d = uint8(repmat(iris.visible_mask_closed, 1, 1, 3));

% Create a yellow 3D border to apply to the RGB image
border = uint8(iris.visible_border);
border(border==1)=255;
border(border==0)=1;
iris_border_3d = repmat(border, 1, 1, 2);
iris_border_3d(:,:,3) = ones(h, w);

% Create a red 3D "correction" border from the iris edges to apply to the RGB image
border2 = uint8(iris.visible_border_corr);
border2(border2==1)=255;
border2(border2==0)=1;
iris_border2_3d(:,:,1) = border2;
iris_border2_3d(:,:,2:3) = repmat(ones(h, w), 1, 1, 2);

% Apply border to the RGB original image
iris.out = img .* iris_border_3d .* iris_border2_3d;
if(verbose), figure(f), subplot(3,2,6), imshow(iris.out); title('segmented'); end;

return
