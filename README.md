# Iris segmentation framework
___

Code of Iris segmentation framework implemented in [1] and ground truth for [CFD database](http://faculty.chicagobooth.edu/bernd.wittenbrink/cfd/index.html) [2].

### File structure:
```
./
 |
 |- gt/         -> ground truth for CFD database
 |- images/     -> images to segment must be placed here
 |- results/    -> segmented masks and eye images with found iris contour are saved here
 |- results/gt/ -> original ground truth cropped to match the size of the saved segmented mask
 |- functions/  -> necessary functions for the framework
 |- framework code
```

A sample image is include in order to test the framework. To do it, just run the file called **run_iris_segmentation.m**.
 
### More information:

**[1]**    F. Fuentes-Hurtado, V. Naranjo and J. A. Diego-Más, "A Fast and Robust Hybrid Method for Accurate Iris Segmentation". 2016, *in press*.

**[2]**    D.S. Ma, J. Correll and B. Wittenbrink, "The chicago face database: A free stimulus set of faces and norming data", Behavior research methods, vol. 47, no. 4, pp. 1122-1135, 2015.

**[3]**    A. Asthana, S. Zafeiriou, S. Cheng and M. Pantic., "Incremental Face Alignment in the Wild", CVPR 2014.