function iris_wo_sr = remove_iris_specular_reflections(img, mask, th_sr_mult, sigma)
% Remove iris' specular reflections

debug = false;

[h, w] = size(mask);

% fast and not too good
% iris_wo_sr = imcomplement(close_holes(imcomplement(img(:,:,1))));

% make mask smaller to dont get white region of the eye
mask = imerode(mask, strel('disk', 2));

iris = img .* repmat(uint8(mask),1,1,3);

iris_gray = iris(:,:,1);

spec_ref_and_eyelid = iris_gray - imgaussfilt(iris_gray, sigma);
th = th_sr_mult*graythresh(spec_ref_and_eyelid); 
% cprintf('_green', 'trial th: line 17 remove_iris_specular_reflections.m\nv0: graythresh(spec_ref_and_eyelid);\nv1: th = 0.1;\n0.75*graythresh(spec_ref_and_eyelid);\n'); %graythresh(spec_ref_and_eyelid);
spec_ref_and_eyelid_otsu = im2bw(spec_ref_and_eyelid, th);

spec_ref_and_eyelid_mask = ~spec_ref_and_eyelid_otsu;

iris_gray_wo_sr_eyelid = iris_gray .* uint8(spec_ref_and_eyelid_mask);

color_mean = round(mean(iris_gray_wo_sr_eyelid(iris_gray_wo_sr_eyelid>0)));
s = regionprops(spec_ref_and_eyelid_otsu, 'Eccentricity', 'Centroid', 'Area', 'PixelIdxList');
sm = regionprops(mask, 'BoundingBox');
hm = sm.BoundingBox(4);
middle = sm.BoundingBox(2) + hm/2;
bottom = sm.BoundingBox(2) + hm;

% areas with low eccentricity are similar to circles
ecc = [s(:).Eccentricity];
centroids = reshape([s(:).Centroid]', 2, numel(ecc));
areas = [s(:).Area];
% bbox = [s(:).BoundingBox];


% this is to get circular specular reflexions
ecc_ok = ecc <= 0.75 & areas >= pi*3^2 & areas <= pi*10^2;
% this to get blank regions
bb_ok = ecc > 0.75 & areas >= pi*3^2 & areas <= 0.33*pi*(hm/2)^2 & centroids(2,:) > 0.8*middle & centroids(2,:) < 0.8*bottom;

whites_ok = ecc_ok | bb_ok;

if(sum(whites_ok(:)))
    spec_ref_pxls = {s(whites_ok).PixelIdxList};
    spec_ref_pxls = cell2mat(spec_ref_pxls');
    mask_spec_ref = false(h,w);
    mask_spec_ref(spec_ref_pxls) = true;

    % make a little bigger
    mask_spec_ref_bigger = imdilate(mask_spec_ref, strel('disk', 2));

    % now, finally remove specular reflexions
    iris_wo_sr = iris_gray;
    iris_wo_sr(mask_spec_ref_bigger>0) = color_mean;
    iris_wo_sr = iris_wo_sr .* uint8(mask); 

    % insert the iris wo spec red into the original image
    imgg = img(:,:,1);
    ind_wo_sr = iris_wo_sr == 0;
    iris_wo_sr(ind_wo_sr) = imgg(ind_wo_sr);
else
    iris_wo_sr = imcomplement(close_holes(imcomplement(img(:,:,1))));
end

% time to remove eyelid
% iris_wo_sr_and_eyelid

if(debug)
    fr = figure; title('specular reflexion and eyelids');
    subplot(3,1,1), imshow(spec_ref_and_eyelid), title('gaussian to find eyelids and spec ref');
    subplot(3,1,2), imshow(spec_ref_and_eyelid_otsu), title('otsu');
    subplot(3,1,3), imshow(iris_wo_sr), title('spec ref removed');
    close(fr)
end
% subplot(4,1,3), imshow(iris_wo_sr_and_eyelid), title('eyelids removed');