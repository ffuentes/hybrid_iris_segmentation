% ####################################################################### %
% ##########################  GET LANDMARKS ############################# %
% ####################################################################### %
%                                                                         %
% First: use Chehra algorithm for facial features detection               %
%                                                                         %
% ####################################################################### %
function [landmarks, bbox] = getLandmarksFromImg(img, image_name, models, verbose)

% % Perform Fitting
% % Load Image
test_image = im2double(img);
% if(verbose)
%     figure1 = figure;
%     imshow(test_image);
%     hold on;
% end

% % Resize image to speed up the process
size_h = size(test_image, 2);
size_resized_h = 512;
if(size_h > size_resized_h)
    ratio = size_resized_h / size_h;
    test_image_res = imresize(test_image, ratio);
else
    ratio = 1;
    test_image_res = test_image;
end

% % Get landmarks and bbox with Chehra algorithm - accurate
[landmarks_res, bbox_res] = getLandmarksChehra( test_image_res, ...
                                                image_name, ...
                                                models.refShape, ...
                                                models.RegMat, ...
                                                verbose);

% % If CHEHRA cannot detect face, discard image
if(landmarks_res == -1)
    disp('Image skipped. Couldn''t detect any face.');
    return;
end

% % Relocate landmarks in original sized image
landmarks = landmarks_res / ratio;
bbox = bbox_res / ratio;