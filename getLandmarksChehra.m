function [landmarks, bbox] = getLandmarksChehra(test_image, image_name, refShape, RegMat, verbose, folder)

% % Detect faces
disp(['CHEHRA: Detecting Face in ' ,image_name]);

faceDetector = vision.CascadeObjectDetector();
bbox = step(faceDetector, test_image);
bbox = bbox';

% % If more than one face detected, keep the biggest one. If no face is
% detected, return -1
n_bbox = size(bbox,2);
if(isempty(bbox))
    landmarks = -1;
    bbox = -1;
    return;
end
if(n_bbox > 1)
    if(verbose)
        colors = {'blue', 'green', 'cyan', 'magenta', 'yellow', 'black', 'white', ...
            'blue', 'green', 'cyan', 'magenta', 'yellow', 'black', 'white', ...
            'blue', 'green', 'cyan', 'magenta', 'yellow', 'black', 'white'};
        for nb=1:n_bbox
            test_image = insertShape(test_image, 'rectangle', bbox(:,nb)', 'Color', colors{nb});
        end
    end
    sizes = bbox(3,:).*bbox(4,:);
    bbox = bbox(:,sizes == max(sizes));
end

% % draw face detector's rectangle
if (verbose)
    test_image = insertShape(test_image, 'rectangle', bbox', 'Color', 'red');
    figure, imshow(test_image);
end

test_init_shape = InitShape(bbox,refShape);
test_init_shape = reshape(test_init_shape,49,2);

% % first approximation of landmarks: by relocating the model according
% % to the face detection location
if(verbose)
    hold on;
    plot(test_init_shape(:,1),test_init_shape(:,2),'ro');
    % plot numbers
    c = strsplit(num2str(1:numel(test_init_shape(:,1))), ' ');
    dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
    text(test_init_shape(:,1)+dx, test_init_shape(:,2)+dy, c);
    title('Landmark''s Initialization');
end

if size(test_image,3) == 3
    test_input_image = im2double(rgb2gray(test_image));
else
    test_input_image = im2double(test_image);
end

disp(['CHEHRA: Fitting ' image_name]);
% % Maximum Number of Iterations
% % 3 < MaxIter < 7
MaxIter=6;
landmarks = Fitting(test_input_image,test_init_shape,RegMat,MaxIter);

% % plot fitted landmarks
if(verbose)
    figure, imshow(test_image); hold on;
    plot(landmarks(:,1),landmarks(:,2),'g.');
     % plot numbers
    % c = strsplit(num2str(1:numel(test_init_shape(:,1))), ' ');
    % dx = 0.1; dy = 0.1; % displacement so the text does not overlay the data points
    % text(test_init_shape(:,1)+dx, test_init_shape(:,2)+dy, c);
    title('Landmark''s fitted')
end