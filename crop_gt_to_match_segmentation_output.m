function gts = crop_gt_to_match_segmentation_output(gt, masks, filename)
% ORIGINAL SIZE GT

% Create mask for both eyes
e_mask = uint8(masks.mask_RE | masks.mask_LE);

% Convert image to gray and apply eye's mask
% img_gray = rgb2gray(img) .* e_mask;
% Opponent color works worse for iris contour (circle)

% Find bounding box to crop the image
props = regionprops(logical(e_mask), 'BoundingBox');

% Find coordinates
x1= floor(props(1).BoundingBox(2)); dx1 = ceil(props(1).BoundingBox(4));
y1 = floor(props(1).BoundingBox(1)); dy1 = ceil(props(1).BoundingBox(3));
x2 = floor(props(2).BoundingBox(2)); dx2 = ceil(props(2).BoundingBox(4));
y2 = floor(props(2).BoundingBox(1)); dy2 = ceil(props(2).BoundingBox(3));

% % Correct landmarks for cropped image with only eyes
% er_lm = landmarks(37:42, :) - repmat([y1 x1], 6, 1);
% el_lm = landmarks(43:48, :) - repmat([y2 x2], 6, 1);

% Crop images (gray and color)
gts.er = gt(x1:x1+dx1, y1:y1+dy1, :);
gts.el = gt(x2:x2+dx2, y2:y2+dy2, :);
% er = img(x1:x1+dx1, y1:y1+dy1, :);
% el = img(x2:x2+dx2, y2:y2+dy2, :);

% % Save the final detection
imwrite(gts.el, [filename '_L.bmp']);
imwrite(gts.er, [filename '_R.bmp']);